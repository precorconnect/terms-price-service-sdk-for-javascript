import {inject} from 'aurelia-dependency-injection';
import TermsPriceServiceSdkConfig from './termsPriceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import TermsWebView from './termsWebView'

@inject(TermsPriceServiceSdkConfig, HttpClient)
class ExtendedWarrantyTermsFeature {

    _config:TermsPriceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:TermsPriceServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param {string} accessToken
     * @returns {Promise.<TermsWebView[]>}
     */
    execute(accessToken:string):Promise<TermsWebView[]> {

        return this._httpClient
            .createRequest('extendedWarranty-terms-price/extendedWarrantyTerms')
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                Array.from(response.content,
                   contentItem =>
                       new TermsWebView(
                           contentItem.term
                       )
               )
            )

    }

}
export default ExtendedWarrantyTermsFeature;