import TermsPriceServiceSdkConfig from './termsPriceServiceSdkConfig';
import DiContainer from './diContainer';
import ExtendedWarrantyTermsFeature from './extendedWarrantyTermsFeature';
import TermsWebView from './termsWebView';
import ExtendedWarrantyRequestRuleWebView from './extendedWarrantyRequestRuleWebView';
import ExtendedWarrantyResponseRuleWebView from './extendedWarrantyResponseRuleWebView';
import SearchExtendedWarrantyTermsPriceFeature from './searchExtendedWarrantyTermsPriceFeature';


/**
 * @class {TermsPriceServiceSdk}
 */
export default class TermsPriceServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {TermsPriceServiceSdkConfig} config
     */
    constructor(config:TermsPriceServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} accessToken
     * @returns {Promise.<TermsWebView[]>}
     */
    extendedWarrantyTerms(
        accessToken:string):Promise<TermsWebView[]>{

        return this
            ._diContainer
            .get(ExtendedWarrantyTermsFeature)
            .execute(
                accessToken
            );
    }

    /**
     * @param {ExtendedWarrantyRequestRuleWebView} request
     * @param {string} accessToken
     * @returns {ExtendedWarrantyResponseRuleWebView}
     */
    searchExtendedWarrantyTermsPrice(
        request:ExtendedWarrantyRequestRuleWebView,
        accessToken:string):Promise<ExtendedWarrantyResponseRuleWebView> {

        return this
            ._diContainer
            .get(SearchExtendedWarrantyTermsPriceFeature)
            .execute(
                request,
                accessToken
            );
    }

}
