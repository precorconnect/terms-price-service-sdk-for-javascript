import ExtendedWarrantyResponseRuleSimpleSerialCodeWebView from './extendedWarrantyResponseRuleSimpleSerialCodeWebView';
import ExtendedWarrantyResponseRuleCompositeSerialCodeWebView from './extendedWarrantyResponseRuleCompositeSerialCodeWebView';


/**
 * @class {ExtendedWarrantyResponseRuleWebView}
 */
export default class ExtendedWarrantyResponseRuleWebView {

    _simpleSerialCode:ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[];

    _compositeSerialCode:ExtendedWarrantyResponseRuleCompositeSerialCodeWebView[];

    constructor(
        simpleSerialCode:ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[],
        compositeSerialCode:ExtendedWarrantyResponseRuleCompositeSerialCodeWebView[]
    ) {

        this._simpleSerialCode = simpleSerialCode;

        this._compositeSerialCode = compositeSerialCode;
    }

    /**
     * getter methods
     */
    get simpleSerialCode():ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[] {
        return this._simpleSerialCode;
    }

    get compositeSerialCode():ExtendedWarrantyResponseRuleCompositeSerialCodeWebView[] {
        return this._compositeSerialCode;
    }


    toJSON(){
        return {
            simpleSerialCode: this._simpleSerialCode,
            compositeSerialCode: this._compositeSerialCode
        }
    }
}
