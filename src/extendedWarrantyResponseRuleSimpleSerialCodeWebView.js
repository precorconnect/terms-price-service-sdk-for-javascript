/**
 * @class {ExtendedWarrantyResponseRuleSimpleSerialCodeWebView}
 */
export default class ExtendedWarrantyResponseRuleSimpleSerialCodeWebView {


    _serialNumber:string;

    _term:string;

    _price:number;

    _materialNumber:string;

    _isEWeligible:boolean;


    /**
     * @param {string} serialNumber
     * @param {string} term
     * @param {number} price
     * @param {string} materialNumber
     * @param {boolean} isEWeligible
     */
    constructor(
        serialNumber:string,
        term:string,
        price:number,
        materialNumber:string,
        isEWeligible:boolean
    ) {
        this._serialNumber = serialNumber;

        this._term = term;

        this._price = price;

        this._materialNumber = materialNumber;

        this._isEWeligible = isEWeligible;
    }


    /**
     * @getter methods
     */
    get serialNumber(): string {
        return this._serialNumber;
    }

    get term(): string {
        return this._term;
    }

    get price(): number {
        return this._price;
    }

    get materialNumber(): string {
        return this._materialNumber;
    }

    get isEWeligible(): boolean  {
        return this._isEWeligible;
    }


    toJSON() {
        return {
            serialNumber : this._serialNumber,
            term : this._term,
            price : this._price,
            materialNumber : this._materialNumber,
            isEWeligible : this._isEWeligible
        }
    }
}
