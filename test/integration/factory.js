import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';

import ExtendedWarrantyRequestRuleWebView from '../../src/extendedWarrantyRequestRuleWebView';
import ExtendedWarrantyRequestRuleCompositeSerialCodeWebView from '../../src/extendedWarrantyRequestRuleCompositeSerialCodeWebView';
import ExtendedWarrantyRequestRuleSimpleSerialCodeWebView from '../../src/extendedWarrantyRequestRuleSimpleSerialCodeWebView';


export default {
    constructValidPartnerRepOAuth2AccessToken,
    constructValidAppAccessToken,
    constructSimpleLineItems,
    constructCompositeLineItems
}

function constructValidPartnerRepOAuth2AccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.url,
        "account_id": dummy.partnerAccountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructSimpleLineItems():ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[]{

    return [{
        serialNumber:dummy.serialNumber
        }];
}

function constructCompositeLineItems():ExtendedWarrantyRequestRuleCompositeSerialCodeWebView[]{

    return [{
        components:constructSimpleLineItems()
    }];
}

